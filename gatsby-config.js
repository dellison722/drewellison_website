require(`dotenv`).config({
  path: `.env`,
})

const shouldAnalyseBundle = process.env.ANALYSE_BUNDLE;
const siteAddress = new URL("https://www.drewellison.com");

module.exports = {
  siteMetadata: {
    siteTitle: 'Drew Ellison Blog',
    siteTitleAlt: `Drew Ellison Blog`,
    siteUrl: `https://www.drewellison.com`,
    siteDescription: `A place where Drew rants about software design, coffee, and his dog.`,
    siteImage: `/banner.jpg`,
    siteLanguage: `en`,
    author: `Drew Ellison`
  },
  plugins: [
    {
      resolve: `@lekoarts/gatsby-theme-minimal-blog`,
      // See the theme's README for all available options
      options: {
        navigation: [
          {
            title: `Blog`,
            slug: `/blog`,
          },
          {
            title: `About`,
            slug: `/about`,
          },
        ],
        externalLinks: [
          {
            name: `Github`,
            url: `https://www.github.com/dme722`
          },
          {
            name: `Buy Me a Coffee`,
            url: `https://www.buymeacoffee.com/drewellison`
          }
        ],
      },
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: process.env.GOOGLE_ANALYTICS_ID,
      },
    },
    `gatsby-plugin-sitemap`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Drew Ellison Blog`,
        short_name: `Drew Ellison Blog`,
        description: `A place where Drew rants about software design, coffee, and his dog.`,
        start_url: `/`,
        background_color: `#fff`,
        theme_color: `#6B46C1`,
        display: `standalone`,
        icons: [
          {
            src: `/android-chrome-192x192.png`,
            sizes: `192x192`,
            type: `image/png`,
          },
          {
            src: `/android-chrome-512x512.png`,
            sizes: `512x512`,
            type: `image/png`,
          },
        ],
      },
    },
    `gatsby-plugin-offline`,
    `gatsby-plugin-gatsby-cloud`,
    `gatsby-plugin-netlify`,
    shouldAnalyseBundle && {
      resolve: `gatsby-plugin-webpack-bundle-analyser-v2`,
      options: {
        analyzerMode: `static`,
        reportFilename: `_bundle.html`,
        openAnalyzer: false,
      },
    },
    {
      resolve: `gatsby-plugin-s3`,
      options: {
          bucketName: "drewellison-website",
          protocol: siteAddress.protocol.slice(0, -1),
          hostname: siteAddress.hostname,
      },
  },
  {
    resolve: `gatsby-plugin-canonical-urls`,
    options: {
        siteUrl: siteAddress.href.slice(0, -1),
    }
}
  ].filter(Boolean),
}
